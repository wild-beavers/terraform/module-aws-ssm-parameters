## 6.1.0

- feat: make `version` output optional using `var.output_versions_enabled` to deprecate this output

## 6.0.2

- maintenance: migrate module to Terraform registry
- chore: bump pre-commit hooks

## 6.0.1

- fix: adds tags to policies created by this module
- chore: bump pre-commit hooks
- doc: updates CHANGELOG to follow conventions

## 6.0.0

- feat: (BREAKING) remove `overwrites` variable as it's deprecated since aws provider `5`
- fix: removed deprecated `ssm_parameter` overwrite
- maintenance: use `aws_provider` version >=5

## 5.0.1

- fix: enables `ssm:LabelParameterVersion` for read/write policy for SSM

## 5.0.0

- tech: (BREAKING) replace deprecated `source_json` to `source_policy_documents` in `aws_iam_policy_document` data. This requires AWS provider >= 4.0
- fix: adds `iam_policy_sid_prefix` and `iam_policy_sid_suffix` variables to avoid sid unique issue when using multiple json policy with `source_policy_documents`
- chore: change pre-commit git hooks scheme
- chore: bump pre-commit hooks
- chore: adds LICENSE file
- refactor: various changes due to TF lint issue

## 4.1.0

- feat: adds `var.kms_key_policy`
- test: adds `assume_role` for all examples

## 4.0.0

- feat (BREAKING): removes `var.parameters_count` as it's not relevant in recent version of Terraform
- chore (BREAKING): sets Terraform min version to 0.15
- chore (BREAKING): sets aws provider min version to 3.25
- refactor (BREAKING): removes `var.enabled` as it's not needed since Terraform 0.13
- refactor: disabling the module does not require any variables anymore
- chore: removes Jenkinsfile
- chore: revamps .gitignore
- maintenance: bumps .pre-commit-config versions

## 3.0.4

- fix: count on `data "aws_iam_policy_document" "kms_key_*"`, don't run when `! var.use_default_kms_key`
- fix: change version constraint for aws provider (only `>= 2.47`)
- chore: for examples, move providers versions to `versions.tf`
- chore: for examples, move providers to `providers.tf`

## 3.0.3

- fix: describe type for name prefix
- fix: set default to null instead of empty string
- chore: bump pre-commit

## 3.0.2

- fix: in versions.tf change from `~> 2.47` to `>= 2.47, < 4.0`
- chore: bump pre-commit hooks

## 3.0.1

- fix: typo in versions.tf to be usable with terraform 0.13

## 3.0.0

- feat: (BREAKING) Rename overwrite and add overwrites variables

## 2.0.1

- fix: Prevent null resource on IAM policy without KMS key

## 2.0.0

- fix: Add `parameters_count` variable. You must update you module with this variable, otherwise, no SSM paramters will be created. This fix a terraform 0.12 pre-processor issue.
- tech: Remove `kms_key_id` variable. SSM parameter resource can now accept both KMS key ID or KMS key ARN.
- tech: Remove old limitation when trying to switch from `SecureString` to `String` since AWS provider 2.47.0 fix an issue [#10819](https://github.com/terraform-providers/terraform-provider-aws/pull/10819)

## 1.1.0

- feat: allow default KMS key
- fix: KMS key outputs form list to string. WARNING, THIS IS A BREAKING CHANGE
- fix: module outputs plural now singular: `kms_key_arn`, `kms_key_id` and `kms_alias_arn`. WARNING, THIS IS A BREAKING CHANGE.
- fix: fix terraform pre-processor issue when use a `kms_key_id`
