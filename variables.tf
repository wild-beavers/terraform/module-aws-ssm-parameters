#####
# Global variables
####

variable "tags" {
  description = "Global tags for resources"
  type        = map(string)
  default     = {}
}

variable "output_versions_enabled" {
  description = "Whether to output versions or not. This output will be removed in next major version to avoid idempotency issue."
  type        = bool
  default     = true
  nullable    = false
}

#####
# SSM parameters
#####

variable "prefix" {
  description = "The prefix to be used for every SSM Parameters. The prefix must match [A-Za-z0-9/]"
  type        = string
  default     = ""
}

variable "names" {
  description = "List of names for parameters."
  type        = list(string)
  default     = []
}

variable "descriptions" {
  description = "List of descriptions for parameters."
  type        = list(string)
  default     = []
}

variable "types" {
  description = "List of types for parameters."
  type        = list(string)
  default     = []
}

variable "values" {
  description = "List of values for parameters."
  type        = list(string)
  default     = []
}

variable "ignore_changes_on_value" {
  description = "Whether or not to ignore changes made manually on the value. Applies to all specified parameters. If set to `true`, terraform will never update the value."
  type        = bool
  default     = false
}

variable "allowed_patterns" {
  description = "List of regular expression used to validate the parameter value."
  type        = list(string)
  default     = []
}

#####
# KMS key
#####

variable "kms_key_create" {
  description = "Create a kms key for secure string parameters."
  type        = bool
  default     = false
}

variable "kms_key_arn" {
  description = "ARN of the kms key if toggle kms_key_create is disable."
  type        = string
  default     = ""
}

variable "kms_key_name" {
  description = "Name of the kms key if toggle kms_key_create is set"
  type        = string
  default     = ""
}

variable "kms_tags" {
  description = "Tags that will be merged with variable tags for the kms key"
  type        = map(string)
  default     = {}
}

variable "kms_key_alias_name" {
  description = "Alias of the kms key if toggle kms_key_create is set"
  type        = string
  default     = ""
}

variable "kms_key_policy" {
  description = "A valid policy JSON document. Although this is a key policy, not an IAM policy, an aws_iam_policy_document, in the form that designates a principal, can be used. For more information about building policy documents with Terraform, see the [AWS IAM Policy Document Guide](https://learn.hashicorp.com/terraform/aws/iam-policy). Will be ignored if `var.kms_key_create` is `false`."
  type        = string
  default     = null
}

variable "use_default_kms_key" {
  description = "Use default kms_key"
  type        = bool
  default     = false
}

#####
#  IAM Policy
#####

variable "iam_policy_create" {
  description = "Create read only and read write policy to get an access to SSM paramters"
  type        = bool
  default     = false
}

variable "iam_policy_name_prefix_read_only" {
  description = "Name of the IAM read only access to SSM parameter policy"
  type        = string
  default     = null
}

variable "iam_policy_name_prefix_read_write" {
  description = "Name of the IAM read write access to SSM parameter policy"
  type        = string
  default     = null
}

variable "iam_policy_path" {
  description = "Path in which to create the policies."
  type        = string
  default     = "/"
}

variable "iam_policy_sid_prefix" {
  description = "Prefix to add in the policies statements SID. This can be useful to avoid SID duplication issue."
  type        = string
  default     = ""
  nullable    = false

  validation {
    error_message = "“iam_policy_sid_prefix” must match “\\w”."
    condition     = can(regex("\\w*", var.iam_policy_sid_prefix))
  }
}

variable "iam_policy_sid_suffix" {
  description = "Suffix to add in the policies statements SID. This can be useful to avoid SID duplication issue."
  type        = string
  default     = ""
  nullable    = false

  validation {
    error_message = "“iam_policy_sid_suffix” must match “\\w”."
    condition     = can(regex("\\w*", var.iam_policy_sid_suffix))
  }
}
