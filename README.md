terraform-module-aws-ssm-parameters

Generic module to creates SSM Parameters for AWS.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.15 |
| aws | >= 5.0 |

## Providers

| Name | Version |
|------|---------|
| aws | >= 5.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_iam_policy.read_only](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_policy.read_write](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_kms_alias.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_alias) | resource |
| [aws_kms_key.this](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/kms_key) | resource |
| [aws_ssm_parameter.do_not_ignore_changes_on_value](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssm_parameter) | resource |
| [aws_ssm_parameter.ignore_changes_on_value](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssm_parameter) | resource |
| [aws_caller_identity.current](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/caller_identity) | data source |
| [aws_iam_policy_document.kms_key_read_only](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.kms_key_read_write](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.read_only](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [aws_iam_policy_document.read_write](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| allowed\_patterns | List of regular expression used to validate the parameter value. | `list(string)` | `[]` | no |
| descriptions | List of descriptions for parameters. | `list(string)` | `[]` | no |
| iam\_policy\_create | Create read only and read write policy to get an access to SSM paramters | `bool` | `false` | no |
| iam\_policy\_name\_prefix\_read\_only | Name of the IAM read only access to SSM parameter policy | `string` | `null` | no |
| iam\_policy\_name\_prefix\_read\_write | Name of the IAM read write access to SSM parameter policy | `string` | `null` | no |
| iam\_policy\_path | Path in which to create the policies. | `string` | `"/"` | no |
| iam\_policy\_sid\_prefix | Prefix to add in the policies statements SID. This can be useful to avoid SID duplication issue. | `string` | `""` | no |
| iam\_policy\_sid\_suffix | Suffix to add in the policies statements SID. This can be useful to avoid SID duplication issue. | `string` | `""` | no |
| ignore\_changes\_on\_value | Whether or not to ignore changes made manually on the value. Applies to all specified parameters. If set to `true`, terraform will never update the value. | `bool` | `false` | no |
| kms\_key\_alias\_name | Alias of the kms key if toggle kms\_key\_create is set | `string` | `""` | no |
| kms\_key\_arn | ARN of the kms key if toggle kms\_key\_create is disable. | `string` | `""` | no |
| kms\_key\_create | Create a kms key for secure string parameters. | `bool` | `false` | no |
| kms\_key\_name | Name of the kms key if toggle kms\_key\_create is set | `string` | `""` | no |
| kms\_key\_policy | A valid policy JSON document. Although this is a key policy, not an IAM policy, an aws\_iam\_policy\_document, in the form that designates a principal, can be used. For more information about building policy documents with Terraform, see the [AWS IAM Policy Document Guide](https://learn.hashicorp.com/terraform/aws/iam-policy). Will be ignored if `var.kms_key_create` is `false`. | `string` | `null` | no |
| kms\_tags | Tags that will be merged with variable tags for the kms key | `map(string)` | `{}` | no |
| names | List of names for parameters. | `list(string)` | `[]` | no |
| output\_versions\_enabled | Whether to output versions or not. This output will be removed in next major version to avoid idempotency issue. | `bool` | `true` | no |
| prefix | The prefix to be used for every SSM Parameters. The prefix must match [A-Za-z0-9/] | `string` | `""` | no |
| tags | Global tags for resources | `map(string)` | `{}` | no |
| types | List of types for parameters. | `list(string)` | `[]` | no |
| use\_default\_kms\_key | Use default kms\_key | `bool` | `false` | no |
| values | List of values for parameters. | `list(string)` | `[]` | no |

## Outputs

| Name | Description |
|------|-------------|
| arns | ARNs of SSM Parameters |
| iam\_policy\_read\_only\_arn | ARN of the read only policy |
| iam\_policy\_read\_only\_description | The description of the read only policy |
| iam\_policy\_read\_only\_id | ID of the read only policy |
| iam\_policy\_read\_only\_name | The name of the read only policy |
| iam\_policy\_read\_only\_path | Path of the read only policy |
| iam\_policy\_read\_only\_policy | The policy document |
| iam\_policy\_read\_write\_arn | ARN of the read write policy |
| iam\_policy\_read\_write\_description | The description of the read write policy |
| iam\_policy\_read\_write\_id | ID of the read write policy |
| iam\_policy\_read\_write\_name | The name of the read write policy |
| iam\_policy\_read\_write\_path | Path of the read write policy |
| iam\_policy\_read\_write\_policy | The policy document |
| kms\_alias\_arn | The Amazon Resource Name (ARN) of the key alias |
| kms\_alias\_target\_key\_arn | The Amazon Resource Name (ARN) of the target key identifier |
| kms\_key\_arn | The Amazon Resource Name (ARN) of the key |
| kms\_key\_id | Globally unique identifier for the key |
| names | Names of SSM Parameters |
| types | Types of SSM parameters |
| versions | Versions of SSM parameters |
<!-- END_TF_DOCS -->
